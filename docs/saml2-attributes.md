title: SAML 2.0 Attributes

# Attributes released by Raven SAML 2.0

Following authentication, the IdP on Raven will release various attributes about
the authenticated user. Most of these are derived from the user's
[Lookup](https://www.lookup.cam.ac.uk/) entry.

Each of these attributes has a formal name which appears in the protocol
messages on the wire. When using the Shibboleth service provider software this
is mapped, by the `attribute-map.xml` file, into a more useful id which in turn
is used to make attribute values available to websites.

There are two common sets of formal names in use: one with names starting
`urn:mace:` and one with names starting `urn:oid:`. In line with recommendations
from the [UK Access Management Federation](https://www.ukfederation.org.uk/),
the Raven IdP uses `urn:oid:` format for SAML 2.0 responses.

Raven SAML 2.0 is intended both for University-managed websites and websites
managed by external entities. The level of detail Raven SAML 2.0 releases about
users differs between *internal* sites (those whose domain names end in
`.cam.ac.uk`) and *external* sites (all other sites).

External sites are given basic information on users including an email address
formatted identifier of the form `[crsid]@cam.ac.uk`, an opaque persistent
identifier and some basic information about whether they are considered a
"member" of the University. Internal sites are given a lot more information.

The attribute definitions used by Raven SAML 2.0 are derived from the
[eduPerson](https://refeds.org/eduperson) and
[inetOrgPerson](https://tools.ietf.org/html/rfc2798) object classes.

## All service providers

Raven will release the following to any service provider registered with the
[Metadata application](https://metadata.raven.cam.ac.uk/).

<table>
  <tr align="left" valign="top">
    <th style="width: 35ex;">
      Name
    </th>
    <th>
      Display Name
    </th>
    <th>
      Description
    </th>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.5923.1.1.1.6</code>
    </td>
    <td>
      Principal name (eduPersonPrincipalName)
    </td>
    <td>
      A unique persistent user identifier which is consistent across all
      services. This is based on the user's CRSid and is formatted as an email
      address, <code>[crsid]@cam.ac.uk</code>.

      <p>
        Raven SAML 2.0 makes no guarantee that this email address has an
        associated mailbox and you should not assume that email set to this
        address will be delivered.
      </p>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.5923.1.1.1.9</code>
    </td>
    <td>
      Scoped affiliation (eduPersonScopedAffiliation)
    </td>
    <td>
      One or more values indicating the authenticated user's relationship with
      the organisation operating the IdP.

      <p>
        Anyone appearing in <a href="https://www.lookup.cam.ac.uk/">Lookup</a>
        will have the <code>member@cam.ac.uk</code> value.
      </p>

      <p>
        Anyone entitled to the bulk of the electronic resources licensed by the
        University library will have the
        <code>member@eresources.lib.ac.uk</code> value.
      </p>

      <p>
        New values may be added over time. Unknown values should be ignored.
      </p>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.5923.1.1.1.7</code>
    </td>
    <td>
      Entitlement (eduPersonEntitlement)
    </td>
    <td>
      One or more values indicating particular <em>entitlements</em>. The only
      value used at the moment is
      <code>urn:mace:dir:entitlement:common-lib-terms</code> for anyone entitled
      to access the general University Library electronic resource collection.

      <p>
        New values may be added over time. Unknown values should be ignored.
      </p>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.5923.1.1.1.10</code>
    </td>
    <td>
      Anonymous identifier (eduPersonTargetedID)
    </td>
    <td>
      A unique persistent user identifier which is consistent for all accesses
      to a particular service provider by a particular user but which will be
      different for different users and for different services.
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:0.9.2342.19200300.100.1.3</code>
    </td>
    <td>
      E-mail (mail)
    </td>
    <td>
      Synthesised email address of the form <code>[crsid]@cam.ac.uk</code>.
    </td>
    <td>
    </td>
  </tr>
</table>

## Internal service providers

Raven SAML 2.0 will additionally release the following information to any
*internal* service provider. Internal service providers are usually those being
served from a `.cam.ac.uk` domain. Raven SAML 2.0 will only release values that
have at least "University Wide" visibility in Lookup.

!!! danger
    Much of the data in Lookup is under its subject's direct control and so
    should **never** be used for identification or authorisation purposes.

<table>
  <tr align="left" valign="top">
    <th style="width: 35ex;">
      Name
    </th>
    <th>
      Display Name
    </th>
    <th>
      Description
    </th>
    <th>
      Notes
    </th>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.4</code>
    </td>
    <td>
      Surname
    </td>
    <td>
      Single-valued.
    </td>
    <td>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.42</code>
    </td>
    <td>
      Forename
    </td>
    <td>
      Single-valued.
    </td>
    <td>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.3</code>
    </td>
    <td>
      Common Name
    </td>
    <td>
      Registered Name. Single-valued.
    </td>
    <td>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.16.840.1.113730.3.1.241</code>
    </td>
    <td>
      Display Name
    </td>
    <td>
      Single-valued.
    </td>
    <td>
      3
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.12</code>
    </td>
    <td>
      Title
    </td>
    <td>
      Roles or job titles. Multi-valued.
    </td>
    <td>
      3
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.11</code>
    </td>
    <td>
      Organizational unit
    </td>
    <td>
      Institutions as human-friendly names. Multi-values.
    </td>
    <td>
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.5</code>
    </td>
    <td>
      Institution ID
    </td>
    <td>
      Institutions as <a href="https://www.lookup.cam.ac.uk/">Lookup</a>
      "instid"s. Multi-valued. This is not guaranteed to be in the same order as the
      human-friendly names.
    </td>
    <td>
      1
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.30</code>
    </td>
    <td>
      Primary institution ID
    </td>
    <td>
        "Primary" institution as a <a href="https://www.lookup.cam.ac.uk/">Lookup</a>
        "instid" according to the University's "Jackdaw" system. Single-valued.
    </td>
    <td>
      1
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:2.5.4.20</code>
    </td>
    <td>
      Business phone number
    </td>
    <td>
      Contact telephone numbers. Multi-valued.
    </td>
    <td>
      3
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.11</code>
    </td>
    <td>
      Alternative email
    </td>
    <td>
      All contact email addresses listed in Lookup. Multi-valued.
    </td>
    <td>
      1, 3
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.38</code>
    </td>
    <td>
      MIS status
    </td>
    <td>
      Status within the University. Multi-valued. Possible values include
      <code>staff</code> and <code>student</code>.

      <p>
        Individuals can have both values if they appear in the University's
        central HR system <em>and</em> the University's student database.
      </p>

      <p>
        Individuals who are employed by a University institution but are not
        in the University's central HR system will not have the <code>staff</code>
        value set.
      </p>

      <p>
        New values may be added over time. Unknown values should be ignored.
      </p>
    </td>
    <td>
      1
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.22</code>
    </td>
    <td>
      Lookup group ID
    </td>
    <td>
      The <a href="https://www.lookup.cam.ac.uk/">Lookup</a> "groupid"s of the
      groups which the user is a member of. Multi-valued. The availability of
      this attribute is subject to both the user's choice of suppression and the
      group administrator's.

      <p>
        This is not necessarily in the same order as the group names.
      </p>
    </td>
    <td>
      1
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:1.3.6.1.4.1.6822.1.1.57</code>
    </td>
    <td>
      Lookup group mapping
    </td>
    <td>
      The mappings for <a href="https://www.lookup.cam.ac.uk/">Lookup</a> groupids of the
      groups which the user is a member of to their names, in the form <code>id=name</code>.

      <p>
        The value pairs for this attribute match the values of the user's
        <code>urn:oid:1.3.6.1.4.1.6822.1.1.22</code> attributes to the values of their
        <code>urn:oid:1.3.6.1.4.1.6822.1.1.19</code> attributes.
      </p>
    </td>
    <td>
      1
    </td>
  </tr>

  <tr align="left" valign="top">
    <td>
      <code>urn:oid:0.9.2342.19200300.100.1.1</code>
    </td>
    <td>
      CRSid / User ID
    </td>
    <td>
      Centrally-managed University user ID. Also known as "CRSid". Use
      <code>urn:oid:1.3.6.1.4.1.5923.1.1.1.6</code> in preference.
    </td>
    <td>
      2
    </td>
  </tr>
</table>
<div style="font-size: 75%;">
  <sup>1</sup> A Raven SAML 2.0-only attribute. This is unlikely to be
  meaningful or trustworthy except when asserted by Raven SAML 2.0.
  <br />
  <sup>2</sup> A standard attribute but is used by Raven SAML 2.0 for a specific
  local purpose. This attribute might have different meanings in other identity
  providers.
  <br />
  <sup>3</sup> User-controlled attribute. Never use this attribute for
  identification or authorisation purposes.
</div>

## Customisations for internal service providers

Previously the Raven SAML 2.0 administrators have customised some of these
attributes for particular service providers in the University. This placed
considerable load on the administrators to test that updates to Raven SAML 2.0
did not change behaviour for all the custom service providers. The Raven SAML
2.0 service will not offer custom attribute release to new service providers in
future.

## Attribute release policy

The UIS documents the [Raven SAML
2.0](https://help.uis.cam.ac.uk/service/accounts-passwords/it-staff/raven/raven-shibboleth-service)
attribute release policy for external service providers.
