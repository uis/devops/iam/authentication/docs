title: Overview

# Adding Raven to your applications

If you are developing your own web applications you will usually want to add
support for Raven authentication directly into your site rather than relying on
the web server implementing it for you.

This section includes guides on integrating Raven with popular application
frameworks.

Raven offers two standard protocols: OAuth2 and SAML 2.0. It is highly likely
that your application framework will already have support for at least one of
those protocols.

## Choosing a protocol

For server-side applications, either Raven SAML 2.0 or Raven OAuth2 can be
used and the choice will depend mostly on which protocol is best supported by
your server-side framework.

For client-side applications which run primarily within the end-user's web
browser we recommend Raven OAuth2 as it has been designed from the beginning to
support client-side flows. For the most part you can follow any guide for adding
sign in with Google support such as Google's own documentation on [OAuth2 for
Client-side Web
Applications](https://developers.google.com/identity/protocols/OAuth2UserAgent).

Irrespective of which protocol you select, make sure that you follow the [Raven
"golden rules"](golden-rules.md).

## "Your framework here"

We welcome contributions covering how to integrate Raven sign in with popular
client-side and server-side web frameworks. Please [open an
issue](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/docs/issues/new) on
the Raven documentation project to start a conversation with us.
