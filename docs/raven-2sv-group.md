title: Raven 2SV Lookup group

A [Lookup group](https://help.uis.cam.ac.uk/service/collaboration/lookup/groups) has been
created that contains all the users who have their Raven OAuth2 two-factor authentication
enabled:

> [uis-raven-2sv-users](https://www.lookup.cam.ac.uk/group/uis-raven-2sv-users) - groupID: 105223

This group can be used to restrict authorization via
[ldap-attribute](lookup.md#allowing-only-users-with-raven-2sv-enabled) with Apache's authnz_ldap
module or programmatically using Lookup's
[web service API](https://help.uis.cam.ac.uk/service/collaboration/lookup/ws).

!!! info
    This group is synchronized from the
    [University's Google Workspace](https://help.uis.cam.ac.uk/service/collaboration/workspace)
    every 5 minutes so there may be a short delay between a user enabling their Google 2SV and
    becoming a member of this group.
