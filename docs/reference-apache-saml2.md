title: Apache and Raven SAML 2.0

# Apache configuration for Raven SAML 2.0

This page contains an example Apache configuration for Raven SAML 2.0. For a
detailed walkthrough of how it is used, see the [Apache Raven SAML 2.0
quickstart](apache-saml2.md).

To make use of this configuration, you must have installed
the [Shibboleth 3 Service Provider for
Apache](https://wiki.shibboleth.net/confluence/display/SP3/LinuxInstall).

## Sample configuration

Apache should be configured in the following manner:

```apache
# Apache configuration for default HTTP site.
<VirtualHost *:80>
    # Serve files from a directory on the server
    DocumentRoot /var/www/htdocs

    # The "/Shibboleth.sso" location is "special" and is handled by the
    # Shibboleth module.
    <Location /Shibboleth.sso>
        SetHandler shib
    </Location>

    # Protect entire site with Raven authentication.
    <Location />
        # Use Shibboleth to provide SAML 2.0 integration with Apache.
        AuthType shibboleth

        # Initiate a new session with Raven SAML 2.0 if one did not previously
        # exist.
        ShibRequireSession On

        <RequireAll>
            # Require that authentication succeeded.
            Require valid-user

            # Require that the user is a Cambridge University member. THIS IS
            # REQUIRED TO AVOID LETTING NON-CAMBRIDGE USERS SIGN IN TO YOUR
            # SITE.
            Require shib-attr affiliation member@cam.ac.uk
        </RequireAll>
    </Location>
</VirtualHost>
```

In addition, you'll need to have the following
`/etc/shibboleth/shibboleth2.xml`. Make sure to look through the file carefully
and replace any placeholder sections.

```xml
<SPConfig xmlns="urn:mace:shibboleth:3.0:native:sp:config"
    xmlns:conf="urn:mace:shibboleth:2.0:native:sp:config"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
    xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
    logger="syslog.logger" clockSkew="180">

    <!--
    The ApplicationDefaults element is where most of Shibboleth's SAML bits are defined.
    Resource requests are mapped by the RequestMapper to an applicationId that
    points into to this section (or to the defaults here).
    -->
    <!--  The entityID of this site                                        -->
    <!-- e.g. https://sp.example.org/shibboleth                            -->
    <ApplicationDefaults entityID="REPLACE WITH A NAME FOR YOUR SITE"
                         REMOTE_USER="eppn persistent-id targeted-id">

        <!--
        Controls session lifetimes, address checks, cookie handling, and the protocol handlers.
        You MUST supply an effectively unique handlerURL value for each of your applications.
        The value defaults to /Shibboleth.sso, and should be a relative path, with the SP computing
        a relative value based on the virtual host. Using handlerSSL="true", the default, will force
        the protocol to be https. You should also set cookieProps to "https" for SSL-only sites.
        Note that while we default checkAddress to "false", this has a negative impact on the
        security of your site. Stealing sessions via cookie theft is much easier with this disabled.
        -->
        <Sessions lifetime="28800" timeout="3600" checkAddress="false"
            handlerURL="/Shibboleth.sso" handlerSSL="false" cookieProps="http" relayState="ss:mem"
            exportLocation="http://localhost/Shibboleth.sso/GetAssertion" exportACL="127.0.0.1"
            idpHistory="false" idpHistoryDays="7">

            <!--
            Configures SSO for a default IdP.
            -->
            <SSO entityID="https://shib.raven.cam.ac.uk/shibboleth">
              SAML2
            </SSO>

            <!-- SAML and local-only logout. -->
            <Logout>SAML2 Local</Logout>

            <!-- Extension service that generates "approximate" metadata based on SP configuration. -->
            <Handler type="MetadataGenerator" Location="/Metadata" signing="false"/>

            <!-- Status reporting service. -->
            <Handler type="Status" Location="/Status" acl="127.0.0.1 ::1"/>

            <!-- Session diagnostic service. -->
            <Handler type="Session" Location="/Session" showAttributeValues="false"/>

            <!-- JSON feed of discovery information. -->
            <Handler type="DiscoveryFeed" Location="/DiscoFeed"/>

        </Sessions>

        <!--
        Allows overriding of error template information/filenames. You can
        also add attributes with values that can be plugged into the templates.
        -->
        <!-- The support email address that can be displayed in error messages -->
        <Errors supportContact="your-admin-contact-details@example.com"
            helpLocation="/about.html"
            styleSheet="/shibboleth-sp/main.css"/>

        <!-- Metadata provider -->
        <MetadataProvider type="XML"
            url="https://shib.raven.cam.ac.uk/shibboleth"
            backingFilePath="ucamfederation-idp-metadata-cache.xml"
            reloadInterval="14400">
        </MetadataProvider>

        <!-- Map to extract attributes from SAML assertions. -->
        <AttributeExtractor type="XML" validate="true" reloadChanges="false" path="attribute-map.xml"/>

        <!-- Use a SAML query if no attributes are supplied during SSO. -->
        <AttributeResolver type="Query" subjectMatch="true"/>

        <!-- Default filtering policy for recognized attributes, lets other data pass. -->
        <AttributeFilter type="XML" validate="true" path="attribute-policy.xml"/>

        <!-- Simple file-based resolver for using a single keypair. -->
        <CredentialResolver type="File" key="keys/sp-key.pem" certificate="keys/sp-cert.pem"/>

    </ApplicationDefaults>

    <!-- Policies that determine how to process and authenticate runtime messages. -->
    <SecurityPolicyProvider type="XML" validate="true" path="security-policy.xml"/>

    <!-- Low-level configuration about protocols and bindings available for use. -->
    <ProtocolProvider type="XML" validate="true" reloadChanges="false" path="protocols.xml"/>

</SPConfig>
```

You will also need a set of keys generated for your server. The Apache
Shibboleth Service PRovider software includes a command line utility for this
purpose:

```bash
sudo shib-keygen -o /etc/shibboleth/keys
```

Start your web server and visit
http://myserver.example.com/Shibboleth.sso/Metadata in your browser replacing
`myserver.example.com` with the address of your site.

Open the metadata in a text editor and copy the contents to your clipboard,
visit https://metadata.raven.cam.ac.uk/, sign in and click **Register new
site**.

Provide a descriptive short name for the site, paste the metadata in to the
large box, add a contact email address and click **Submit**.

After around a minute, Raven SAML 2.0 will automatically register your metadata.
You should now be able to visit your site and sign in.
