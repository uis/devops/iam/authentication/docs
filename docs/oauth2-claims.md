# OAuth2 ID Token Claims

When authenticating using Raven OAuth2 all registered clients can ask for basic
profile information on the signed in user via the `openid` and `profile` OAuth2
scopes. The following claims are supported:

|Claim|Description|
|-|-|
|`aud`|The OAuth2 client id of your application|
|`exp`|Expiration time of token as a number of seconds after 1st January 1970|
|`iat`|Issue time of token as a number of seconds after 1st January 1970|
|`sub`|Unique identifier for user. Do **not** attempt to parse this value.|
|`email`|An email address for the current user. For University members this ends `@cam.ac.uk`.|
|`hd`|If present and equal to `cam.ac.uk` this indicates the user id a member of the University.|
|`name`|Human-friendly display name for user|
|`picture`|If present, this is a URL to a profile picture for the user|

## Further reading

Google documents [some additional
claims](https://developers.google.com/identity/protocols/OpenIDConnect#an-id-tokens-payload)
which can appear in the ID token payload.
