title: SAML 2.0

# Generic Raven SAML 2.0 instructions

This page lists various bits of useful information if you want to configure some
third-party application to make use of Raven SAML 2.0.

## Terminology

The SAML 2.0 ecosystem has some terminology associated with it:

* A **service provider** (SP) is a website which wishes to make use of Raven
    SAML 2.0 for sign in.
* The **identity provider** (IdP) is the service providing support for SAML
    2.0-based sign in. In this case it is Raven SAML 2.0.
* **Attributes** are small pieces of information which Raven SAML 2.0 shares
    with service providers about the signed in user.
* **Metadata** is information about a service provider or identity provider
    which is required to be shared between both to enable sign in support.

## Configuring the service provider

There are many SAML 2.0 service providers. It is not possible to include
instructions on configuring all of them. Most settings can be obtained from the
"IdP Metadata URL" at:

```
https://shib.raven.cam.ac.uk/shibboleth
```

If your service provider cannot directly consume metadata, you may need to
specify the "Service URLs" directly. There are two Service URLs for Raven SAML
2.0.

If your service provider wants to use the SAML 2.0 HTTP-POST binding use:

```
https://shib.raven.cam.ac.uk/idp/profile/SAML2/POST/SSO
```

If your service provider wants to use the SAML 2.0 HTTP-Redirect binding use:

```
https://shib.raven.cam.ac.uk/idp/profile/SAML2/Redirect/SSO
```

Raven SAML 2.0 uses a private key to sign responses. Your service provider will
need a copy of the public half of this key embedded within a self-signed
certificate. Usually this is obtained automatically from the metadata but if you
need to upload the certificate manually it is:

```
-----BEGIN CERTIFICATE-----
MIICujCCAaICCQDN9BMM2g2oWzANBgkqhkiG9w0BAQUFADAfMR0wGwYDVQQDExRz
aGliLnJhdmVuLmNhbS5hYy51azAeFw0xNTExMjAxNDUwNTFaFw0yNTExMTcxNDUw
NTFaMB8xHTAbBgNVBAMTFHNoaWIucmF2ZW4uY2FtLmFjLnVrMIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxBNt1CZhNwQfCTD7sT0VctwAsdHAqhOmADg+
Jkpw27QKxVIPlUANAY3e7mbKuWGNYjLv9+KUrkwGhSXnOwUUCC01w+8JpII2j1W6
8iAvKGszfolVfmfj8vqscQ/UqlSKaGjruWk394v3b7eddYh7HCAOtgOJDIKX9F0e
bMkIdqQgw2e5uenwt1S9TgwOvYi+IfuZ5yhQv9Yuwo76QS8UkxOyvZdRZl7MIchx
O0THaTYbrca0GsSc+r9SIb++rM5fQ0yxQzh36PqbGiA1noS/dhkRZb3ywLPNoFzu
qwWOvcN6ubhO5YOKmTPn1N0uVg94LVMCxMWlO+DjZ8aFmMr96wIDAQABMA0GCSqG
SIb3DQEBBQUAA4IBAQBimCfClavq2Wk1Zsq9AQ3TWeVFrm1kaCUi4J5j3uWNlMVK
PsIGE0BHAALMixG+XWt5+QW70CXq6RnHXMS0TLfM5q6K8jIVURK599bTF2/d4fNq
3QJNaVusuqCqym3Z7rt71QfGtPi0rVKVlQL+lL87a0TDLIyWLsbEe786NpYe0mEe
BXPQwpPwSaJ1PnPNlsl5i/cUZou5zZQGHtqEY/PR7wAxS/28A6qWLVpMQEUYtb9M
ZBb6lO15RJ5qwk6paQG87nhMPAFwSbK+OpCkt3hYd7l8LjXNG74eOZdPM5V6DmZz
nMRF0t4QBDKsuZ64N/+u7R3Nj6uzsQsb7PJXGNTf
-----END CERTIFICATE-----
```

You can also download [a copy of the certificate](raven-saml2.crt) from this
site.

### "Unsolicited" or "IdP-initiated" SSO flow

!!! warning
    The Raven team does not recommend or support this flow. It should only be
    used as a last resort.

Some SP implementations may require an "unsolicited" flow since they do not
fully support SP-initiated flows. More information on this use case can be found
in the [Shibboleth
wiki](https://wiki.shibboleth.net/confluence/display/IDP4/UnsolicitedSSOConfiguration).

In this flow, the SP does not request authentication directly but instead
requires that the IdP itself provide authentication information to the SP
without the SP having asked for it.

Use of the unsolicited flow is not recommended and should only be used when
SP-initiated flows are impossible. Raven SAML 2 does not advertise support for
this flow in the IdP metadata.

The unsolicited flow can be triggered by redirecting the user to the following
URL:

```
https://shib.raven.cam.ac.uk/idp/profile/SAML2/Unsolicited/SSO?providerId={entityId}
```

where `{entityId}` should be replaced with the entity id from the SP metadata.

## Attributes

Raven SAML 2.0 releases a very limited set of attributes to all registered
service providers. The [full list](saml2-attributes.md) is available elsewhere
on this site.

## "nameID"s

SAML 2.0 authentications contain a single *nameID* that identifies the
individual being authenticated. A number of different formats of nameID are
defined, corresponding to different ways of identifying individuals.

The Raven SAML 2.0 default, matching normal usage in the Shibboleth ecosystem,
is to use the *transient* nameID format. This creates a random string that
identifies each authentication transaction but which doesn't directly identify
the person being authenticated. In normal Shibboleth usage, nameID is largely
ignored in favour of information provided in [attributes](saml2-attributes.md).

## Uploading metadata

Just as the service provider needs to know something about Raven SAML 2.0 to
make use of it, Raven SAML 2.0 needs some information about the service
provider. This is termed *metadata*. Most service provider implementations
provide some form of automatically generated metadata in the form of an XML
document. This can be registered with Raven SAML 2.0 via the dedicated [metadata
registration application](https://metadata.raven.cam.ac.uk/).

!!! important
    The metadata application expects an XML *snippet*, not a full XML document.
    In practice this means that you need to delete the following from the start
    of the generated metadata if present:

    ```xml
    <?xml version="1.0" encoding="UTF-8" standalone="no" ?>
    ```
