title: Apache and Raven OAuth2

# Apache configuration for Raven OAuth2

This page contains an example Apache configuration for Raven OAuth2. For a
detailed walkthrough of how it is used, see the [Apache Raven OAuth2
quickstart](apache-oauth2.md).

!!! important

    The user experience for Raven OAuth2 may differ from the user experience
    shown by legacy Raven services. In particular users will be asked for
    consent on the first sign in. Please see the [notice to IT
    professionals](https://help.uis.cam.ac.uk/service/support/help-for-institutions/google-cloud-identity)
    on the UIS website for more information.

To make use of this configuration, you must have:

1. Created OAuth2 [client credentials](creating-oauth2-client-credentials.md).
2. Installed [mod_auth_openidc](https://github.com/zmartzone/mod_auth_openidc/).

## Sample configuration

```apache
# Apache configuration for default HTTP site.
<VirtualHost *:80>
    # Serve files from a directory on the server
    DocumentRoot /var/www/htdocs

    # Protect entire site with Raven authentication.
    <Location />
        # Use OIDC authentication.
        AuthType openid-connect

        <RequireAll>
            # Require that authentication succeeded.
            Require valid-user

            # Require that the user be part of Google Workspace for Cambridge.
            #
            # THIS CHECK IS REQUIRED TO STOP ANYONE WITH A GOOGLE ACCOUNT FROM
            # SIGNING IN TO YOUR SITE.
            Require claim "hd:cam.ac.uk"
        </RequireAll>
    </Location>

    # OpenID Connect/OAuth2 configuration

    # Raven OAuth2 is provided by Google. This URL provides metadata which
    # auto-configures some OAuth2 parameters.
    OIDCProviderMetadataURL \
        https://accounts.google.com/.well-known/openid-configuration

    # OAuth2 client application.
    OIDCClientID "some-client-id-you-have-registered"
    OIDCClientSecret "the-client-secret-for-same"

    # Redirect URI claimed by proxy. This should not be any URL used by the
    # site you're protecting. A good choice is usually something like
    # "/.oidc/redirect".
    OIDCRedirectURI "${REDIRECT_URI}"

    # Crypto passphrase for session cookie. This is used to encrypt a session
    # cookie which allows users to re-visit your site once signed in without
    # having to present their Raven credentials again.
    OIDCCryptoPassphrase "some-super-secret-passphrase-you-generate"

    # OAuth2 token request parameters. Use "hd=cam.ac.uk" to request the Raven
    # login box.
    OIDCAuthRequestParams "hd=cam.ac.uk"

    # Scopes indicating the information we want back from Raven.
    OIDCScope "openid email profile"

    # Some sites look at the "remote user" setting in Apache to determine the
    # username which should be used. Set this to the user's "@cam.ac.uk" email
    # address.
    OIDCRemoteUserClaim email
</VirtualHost>
```
