# Firewall configuration

This page documents web resources which should be accessible for Raven sign in
to work. It should be of utility to those looking to determine the "minimal
connectivity" required to complete a Raven sign in.

!!! danger "This page is no longer valid"

    In February 2023, Raven started to use Microsoft Azure Active Directory as a
    backend to authenticate users. Microsoft provides the following guidance on
    URLS and IP address ranges, but these could change at any moment with only
    30 days notice, see [Office 365 URLs and IP address ranges](https://learn.microsoft.com/en-us/microsoft-365/enterprise/urls-and-ip-address-ranges?view=o365-worldwide)

!!! danger "This page is for guidance only"

    This page is provided as a courtesy to those implementing Raven sign in for
    network configuration. It should not be taken as describing a stable
    interface. The details on this page are subject to change. Where possible
    the Raven Admins will endeavour to give advantage notice of changes but this
    will be on a "best-effort" basis.

## Required URLs

It is preferred if network filtering can be performed at the level of request
URLs. In order to allow Raven sign ins, your network must allow requests with
the following base URLs:

* https://accounts.google.com/o/oauth2<sup>1</sup>
* https://core-idp.raven.cam.ac.uk/
* https://legacy.raven.cam.ac.uk/
* https://oauth2.googleapis.com/token<sup>1</sup>
* https://raven.cam.ac.uk/
* https://shib.raven.cam.ac.uk/
* https://www.googleapis.com/oauth2<sup>1</sup>

<sup>1</sup> External requirement imposed by Google. See [Google's
documentation](https://support.google.com/a/answer/9012184?hl=en#zippy=%2Ctop-level-urls)
for more details. Google provides an [online
tool](https://toolbox.googleapps.com/apps/browserinfo/) for checking
connectivity.

!!! question "Why the Google URLs?"

    We are transitioning to using Google's multi-factor authentication (MFA)
    solution for Raven. As more Raven flavours transition, it will become more
    and more difficult to support Raven users with MFA enabled without allowing
    the Google URLs.

You _may_ want to allow HTTP to HTTPS redirect for these URLs although correctly
configured applications should never use a HTTP URL to request resources from
Raven.

## IP addresses

!!! warning

    We do not recommend IP-based filtering unless the firewall can dynamically
    fetch IPs based on the hostnames listed above.

In order to support Raven sign-in traffic must be allowed to the following IPs:

* 34.120.184.55
* 34.117.105.168
* 34.117.93.149
* 128.232.132.4
* 131.111.8.31
* 2001:630:212:8::88:1
* 2600:1901:0:2ddf::

In addition traffic must be allowed to IPs corresponding to the Google hosts
above. Google may change which IPs these hosts resolve to with no notice. If you
want to whitelist all Google IPs, Google [publishes their IP
ranges](https://support.google.com/a/answer/10026322) but in a somewhat
round-about way; they publish a list of [all of their IP
ranges](https://www.gstatic.com/ipranges/goog.json) and [IP ranges which may be
used by their Cloud customers](https://www.gstatic.com/ipranges/cloud.json).
They tell you that the IP ranges for their services can be computed by
subtracting one from another which is non-trivial to do in your head(!)

The following Python 3 script can be run to save you the trouble. It uses only
the standard library and so can be run [via an online Python script
runner](https://www.programiz.com/python-programming/online-compiler/).

```python
--8<-- "get-google-ip-ranges.py"
```

!!! important

    This is very much a case of "sledgehammer to crack a nut". By allowing all
    of the IP ranges reported by this script you are in essence allowing use of
    any of Google's applications. At that point you may want to consider if this
    firewall configuration is providing any meaningful restriction.
