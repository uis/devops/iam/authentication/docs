title: First steps

# Your first steps with Raven

This page will take you through deploying your first Raven protected website.
This includes describing how to register your website with Raven and
demonstrating the sort of information Raven will give you about users signing
into your site.

## Prerequisites

This guide is written assuming you have some familiarity with Linux-like
environments. Specifically, we assume a basic familiarity with the Linux command
line and an understanding of the concept of "environment variables".

## Play with Docker

We won't be installing any software on a server. Instead we'll be making use of
[Play with Docker](https://labs.play-with-docker.com/) to let you launch web
servers to experiment with. Play with Docker requires that you have a Docker
account. You can register one for free from the sign-in page.  After signing in,
click the **Start** button and you should end up with a site that looks like the
following:

<center>
  ![Play with docker](images/play-with-docker.png)
</center>

Click the **add new instance** button and you'll be presented with a standard
Linux command line. We'll only be making use of the one server in this page
but Play with Docker has far more features than we'll be covering here.

!!! tip
    Play with Docker lets you quickly prototype systems where there are multiple
    servers on multiple networks without installing any software on your
    machine. Find out more at https://training.play-with-docker.com/.

## OAuth2 credentials

Raven supports multiple authentication protocols. One of the supported protocols
is called OAuth2 and is the one we'll be using on this page. Raven also provides
the SAML 2.0 protocol which is another popular protocol.

!!! info
    Raven supports a particular flavour of OAuth2 known as [OpenID
    Connect](https://openid.net/connect/). Any service which supports using
    OpenID Connect should be able to be interfaced with Raven.

OAuth2 requires that websites wanting to make use of Raven for authentication
must first identify themselves. This is done by registering some credentials
called a **client id** and a **client secret** with Raven.

Follow the [instructions on registering OAuth2
clients](creating-oauth2-client-credentials.md). Do **not** enter any redirect
URIs or JavaScript origins just yet. We'll be doing this later. Keep a note of
the client id and secret you registered in this step; we will need them soon!

## Starting the example web server

In the Play with Docker terminal window, set the `CLIENT_ID` and `CLIENT_SECRET`
environment variables to the OAuth2 credentials you registered earlier:

```bash
export CLIENT_ID=replace-with-your-client-id

export CLIENT_SECRET=replace-with-your-client-secret
```

We have prepared a Docker container with an example Raven-capable web server
which can make use of your client id and secret. Run it using the following
command:

```bash
docker run --rm -it -e CLIENT_ID -e CLIENT_SECRET -p 8000:80 \
  registry.gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/apache-oauth2
```

This command will download the example web server we have created for you, pass
the values of `CLIENT_ID` and `CLIENT_SECRET` to it as environment variables and
connect up TCP port 80, the HTTP port, to port 8000 of the Play with Docker VM
instance you have launched.

Play with Docker will notice that a new port has been opened when the web server
starts up and will have added a link next to the **open port** button.

Follow the link and... oops!

<center>
  ![It was going so well...](images/google-redirect-uri-error.png)
</center>

Your OAuth2 credentials need to have each website you are going to authenticate
to registered with them. This is an extra guard against someone malicious
obtaining your OAuth2 client credentials and attempting to impersonate your
site. For each site you want to associate with your credentials, you will need
to provide a special *redirect URI*. Play with Docker uses hostnames which
change each time you start an instance so we didn't know what redirect URI to
enter when first creating the OAuth2 client credentials.

Fortunately, this error page gives us both the redirect URI we need to use *and*
the address of the client configuration page. *Copy* and then *paste* the second
URL from the error into your browser's address bar and add the first URL as a
redirect URI. Make sure you save the form after adding the redirect URI.

!!! info
    We don't need any JavaScript origins for this example. These are used when
    you have applications which use JavaScript to talk to Raven directly from
    within your user's web browser instead of having to go via your web server.

Go back to the Play with Docker page and click on the link next to the **open
port** button again. This time you should get a Raven sign-in page and then a
page congratulating you for protecting your first site.

Raven gives you some basic information about the user signing in. This includes
their `@cam.ac.uk` email address, their name and a profile picture if they've
configured it in their Google Workspace for Cambridge profile. The example
website we've created for you lets you see what information Raven sends.

## Next steps

This page has taken you through getting the OAuth2 client credentials you
need to identify your website to Raven. Next we'll look at [configuring
Wordpress](wordpress.md) to use Raven OAuth2.
