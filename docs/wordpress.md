title: Wordpress

# Configuring Raven Authentication in Wordpress

This page follows on from the [first steps page](./first-steps.md) and shows you
how to add Raven authentication to [Wordpress](https://wordpress.org/).
We will be making use of the [OpenID Connect
Generic](https://wordpress.org/plugins/daggerhart-openid-connect-generic/)
plugin for Wordpress. If your particular web application has support for OAuth2
or OpenID Connect it should be relatively easy to add Raven authentication to
it.

## Prerequisites

This guide is written assuming you have completed the [first steps
guide](./first-steps.md) and that you have had some experience with configuring
Wordpress through the admin dashboard.

## Installing Wordpress

We will be making use of Play with Docker as we did on the [first steps
page](./first-steps.md). We've created an docker container with an install of
Wordpress and the OpenID Connect Generic plugin already downloaded which can be
started straight from Play with Docker. In the terminal enter the following
commands:

```bash
wget https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/raw/master/wordpress/docker-compose.yml

docker-compose up
```

!!! tip
    You can examine the docker-compose file and download it from the
    [documentation samples
    project](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/)
    on the University Developers' Hub.

It takes around 30 seconds for Wordpress to download and start. Next to the
**open port** button you will have a link that takes you to your Wordpress site.

You should now be presented with the usual Wordpress install wizard.

1. Select **English (UK)** and click **Continue**.
2. Specify "Test site" as the **Site title**, choose a **Username** for yourself
   and copy the **Password** to use later.
3. Enter your `@cam.ac.uk` address as **Your Email** and click **Install
   Wordpress**.
4. Click the **Log in** button and sign in with the email address and password
   you used earlier.

!!! important "Make sure to use your `@cam.ac.uk` email address"
    It is important that you use your `@cam.ac.uk` email address here as we will
    eventually be making Raven the *only* way to sign in to the site. If the
    email address of the admin doesn't match that of your Raven account you will
    lose access to the site!

## Configure the OpenID Connect plugin

The OpenID Connect plugin cannot auto-configure itself from the Raven OAuth2
metadata URL so we shall have to set some options manually.

Make sure you have [obtained some OAuth2 client
credentials](creating-oauth2-client-credentials.md) before continuing. Do *not*
specify any redirect URIs or JavaScript origins at this point.

1. Choose **Plugins** > **Installed Plugins** from the dashboard menu.
2. Click **Activate** under **OpenID Connect Generic**.
3. Choose **Settings** > **OpenID Connect Client** from the dashboard menu.

Change the following settings leaving all other settings as their defaults.

|Setting|Value|
|-|-|
|**Client ID**|Your OAuth2 client id|
|**Client Secret Key**|Your OAuth2 client secret|
|**OpenID Scope**|`profile email openid`|
|**Login Endpoint URL**|`https://accounts.google.com/o/oauth2/v2/auth?hd=cam.ac.uk`|
|**Userinfo Endpoint URL**|`https://openidconnect.googleapis.com/v1/userinfo`|
|**Token Validation Endpoint URL**|`https://oauth2.googleapis.com/token`|
|**End Session Endpoint URL**|Leave blank|
|**Identity Key**|`email`|
|**Nickname Key**|`name`|
|**Email Formatting**|`{email}`|
|**Display Name Formatting**|`{name}`|
|**Link Existing Users**|checked|

Scroll to the bottom of the page, make a note of redirect URI at bottom and
click **Save Changes**. Take the redirect URI and add it to the OAuth2 client
credentials you created via the [credentials
page](https://console.cloud.google.com/apis/credentials) in the Google
developer console.

## Test sign in

Test that the integration has worked by trying to sign in to your account via
Raven:

1. Sign out by hovering over "Hi, *username*" at the top-right of the screen and
   click **Log out**.
2. Click **Log in with OpenID Connect**.
3. Sign in with Raven.
4. Add `/wp-admin` to the URL in the location bar of your browser and make sure
   you have signed back in as the admin user.

## Disable password-based sign in

Disabling password sign in will show the Raven login box immediately rather than
requiring users to click **Log in with OpenID Connect**.

1. Choose **Settings** > **OpenID Connect Client** from the dashboard menu.
2. Change **Login Type** to **Auto Login - SSO**.
3. Scroll to the bottom of the page and click **Save Changes**.

Now all users will be prompted to sign in via Raven when they chose **Log in**
from the site's meta menu.

!!! danger
    **Never** assume that successful sign in to your site implies a user is
    allowed to view pages on the site. If you want to make sure only Raven users
    can view some pages you **must** restrict those pages to users whose email
    address ends in `@cam.ac.uk` via other Wordpress configuration or plugins.

    Raven does not guarantee that only `@cam.ac.uk` users will be authenticated
    in the future.

## Next steps

This page took you through the process of installing Wordpress, enabling the
OpenID Connect Generic plugin and configuring it for use with Raven.

Remember to read the [Raven golden rules](golden-rules.md) when configuring your
website.

If you want to restrict certain pages of your Wordpress sites to Raven users you
should investigate plugins for Wordpress which lets you restrict content so that
it can only be viewed by users whose email domain is `cam.ac.uk`.
