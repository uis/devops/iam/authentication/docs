title: Apache and Raven SAML 2.0

# Configuring the Apache web server for Raven SAML 2.0

This page follows on from the [first steps page](./first-steps.md) and covers
configuring the [Apache web server](https://httpd.apache.org/) to make use of
Raven SAML 2.0 and require Raven authentication when visiting a site.

## Prerequisites

This guide is written assuming you have completed the [first steps
guide](./first-steps.md), that you have had some experience with the Linux
command line and have configured an Apache web server before.

In order to add Raven authentication to a website you will need to make sure
that the [Shibboleth 3 Service Provider for
Apache](https://wiki.shibboleth.net/confluence/display/SP3/LinuxInstall) is
installed on your server. If you are running a Debian or Ubuntu server, the
[libapache2-mod-shib](https://packages.debian.org/sid/web/libapache2-mod-shib)
package provides this and can be installed via `apt-get`. In this guide we will
make use of a pre-built Docker container based on Ubuntu.

## Quick start

We will be making use of Play with Docker as we did on the [first steps
page](./first-steps.md). If you haven't already, sign in to Play with Docker and
create a new instance.

Much like Raven OAuth2 uses client ids to identify websites, Raven SAML 2.0
uses *entity ids*. These are publicly-visible textual descriptions of sites and
should be unique.

```bash
export ENTITY_ID="raven-docs-experiment-$( \
  dd if=/dev/urandom count=100 of=/dev/stdout 2>/dev/null | sha256sum - | cut -c 1-16)"
```

!!! tip
    This is a bit of Unix command-line trickery which creates a random entity ID
    just for you. In production use, the entity ID is conventionally related to
    the hostname of the site, for example: `https://example.com/shibboleth`.

We have created a Docker image with Apache pre-installed which uses the
[Shibboleth Service
Provider](https://wiki.shibboleth.net/confluence/display/SP3/LinuxInstall) to
interact with Raven SAML 2.0. We'll discuss the configuration later but for now
just start the server:

```bash
docker run --rm -it \
  -e ENTITY_ID -v shib-keys:/etc/shibboleth/keys -p 8000:80 \
  registry.gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/apache-saml2
```

!!! info
    To use Raven SAML 2.0, a website needs to create a "private key" which it
    uses to sign all the requests and to decrypt responses from Raven SAML 2.0.
    Our pre-made image will auto-generate this on first run. We use a *Docker
    volume* called `shib-keys` to store them. This makes sure that the
    key is the same if the server is stopped and re-started.

A link to your new site should have appeared next to the **open port** button.

1. Right-click it the link next to **open port** and copy the URL.
2. Add `/Shibboleth.sso/Metadata` to the URL you copied. So, if the
   URL you copied was http://abc.direct.labs.play-with-docker.com/, visit
   http://abc.direct.labs.play-with-docker.com/Shibboleth.sso/Metadata in your
   browser and save the metadata to a file.
3. Open the metadata in a text editor and copy the contents to your clipboard.
4. Visit https://metadata.raven.cam.ac.uk/, sign in and click **Register new
   site**.
5. Provide a descriptive short name for the site, paste the metadata in to the
   large box and add a contact email address.
6. Click **Submit**.

After around a minute, Raven SAML 2.0 will automatically register your metadata.
You should now be able to click the link next to **open port** and visit your
new site.

## What Raven SAML 2.0 tells you about users

Raven SAML 2.0 is intended both for University-managed websites and websites
managed by others. The level of detail Raven SAML 2.0 releases about users
differs between *internal* sites (those whose domain names end in `.cam.ac.uk`)
and *external* sites (everything else).

External sites are given basic information on users including an email address
formatted identifier of the form `[crsid]@cam.ac.uk`, an opaque persistent
identifier and some basic information about whether they are considered a
member of the University.

Internal sites are given a lot more information including a human-friendly name
for the user and a list of the [Lookup](https://www.lookup.cam.ac.uk/) groups
they are a member of.

!!! info
    There is a separate page giving [the full list of all the attributes
    released by Raven SAML 2.0](saml2-attributes.md).

## A tour of the container

[The source code for our SAML 2.0
container](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/tree/master/apache-saml2)
is publicly available and can be used as a guide to configuring your own Apache
web server to make use of Raven SAML 2.0. We'll go on a quick tour of the
container's contents visiting the most important files.

### Installing mod_shib

The
[Dockerfile](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/blob/master/apache-saml2/Dockerfile)
describes the steps to set up the container. We start from a base Ubuntu
install. We run a number of basic system configuration commands but the main
command which installs and configure mod_shib is:

```bash
apt-get install apache2 libapache2-mod-shib
```

### Shibboleth configuration

The Shibboleth software is configured through two files:
`/etc/shibboleth/attribute-map.xml` and `/etc/shibboleth2/shibboleth2.xml`.

Raven SAML 2.0 can pass information about the current user back to your web
site. These are called *attributes*. The
[attribute-map.xml](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/blob/master/apache-saml2/shib/attribute-map.xml)
file provided in the container tells the Shibboleth software how to map the
somewhat opaque numeric "OID-style" names into something a human can understand.
You will rarely need to modify this file.

The
[shibboleth2.xml](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/blob/master/apache-saml2/shib/shibboleth2.xml.template)
file configures the Shibboleth software. Our container arranges for strings of
the form `${...}` to be replaced with the configuration passed in via
environment variables. In your own deployments you should set these values
directly in the file.

### Configuring mod_shib

The Apache web server supports serving multiple websites from the same server.
Each website is usually configured with a `#!apache <VirtualHost>` section
in the Apache configuration file.

The configuration for our protected site can be found within the
[sites/protected.conf](https://gitlab.developers.cam.ac.uk/uis/devops/iam/authentication/doc-samples/blob/master/apache-saml2/sites/protected.conf)
file. The mod_shib module is configured using directives which start
`Shib...`.

Shibboleth requires that a region of the web-site be set aside for its use. In
our configuration we have set this to be any URL of the form
`/Shibboleth.sso/...` by means of a custom `#!apache <Location>` section:

```apache
# The "/Shibboleth.sso" location is "special" and is handled by the
# Shibboleth module.
<Location /Shibboleth.sso>
    SetHandler shib
</Location>
```

Generally you will not need to change this unless your specific application
requires URLs of the form `/Shibboleth.sso/...`.

### Requiring sign in

Configuring mod_shib does not actually cause the website to be Raven
protected. You must explicitly specify locations within a site which require
sign in. To protect the entire site you can use the following `#!apache
<Location>` section which should be within the appropriate `#!apache
<VirtualHost>` section:

```apache
# Protect entire site with Raven authentication.
<Location />
    # Use Shibboleth to provide SAML 2.0 integration with Apache.
    AuthType shibboleth

    # Initiate a new session with Raven SAML 2.0 if one did not previously
    # exist.
    ShibRequireSession On

    <RequireAll>
        # Require that authentication succeeded.
        Require valid-user

        # Require that the user is a Cambridge University member. THIS IS
        # REQUIRED TO AVOID LETTING NON-CAMBRIDGE USERS SIGN IN TO YOUR SITE.
        Require shib-attr affiliation member@cam.ac.uk

        # If you wish to restrict access to a particular lookup group, set
        # the numeric group number here. Only sites hosted from domains
        # ending in .cam.ac.uk will have lookup groups given to them.
        # Alternatively you could use mod_authz_ldap and use Lookup directly as
        # described in the Raven documentation.
        #
        # Require shib-attr groupID 123456
    </RequireAll>
</Location>
```

## Be a good citizen

Please make sure to **remove** the metadata you added in this quickstart from
the Raven metadata site.

## Next steps

This page has covered the basic moving parts of an Apache web server configured
to use Raven SAML 2.0 for sign in.

Remember to read the [Raven golden rules](golden-rules.md) when configuring your
website.

The [full list of SAML 2.0 attributes](saml2-attributes.md) is available on this
site for your reference along with some [generic Raven SAML 2.0
instructions](raven-saml2.md).
